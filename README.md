# qutecookie

qutecookie is a commandline cookie manager for qutebrowser. qutebrowser doesn't support host selective cookie delete like firefox([related issue](https://github.com/qutebrowser/qutebrowser/issues/58)) which is why I made this :)
