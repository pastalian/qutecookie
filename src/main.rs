mod cookie;

use cookie::Cookie;
use std::env;

fn main() {
    let cookie = Cookie::new();

    let mut args = env::args().into_iter();
    args.next();

    match args.next().as_deref() {
        Some("list") => cookie.print_hosts(),
        Some("show") => {
            if let Some(host) = args.next() {
                cookie.show_host(&host)
            } else {
                print_help()
            }
        }
        Some("delete") => {
            if let Some(host) = args.next() {
                cookie.delete_host(&host)
            } else {
                print_help()
            }
        }
        _ => print_help(),
    }
}

fn print_help() {
    println!("Usage: qutecookie <action> <options>");
    println!("");
    println!("Actions:");
    println!("  help\t\t\tDisplay help text");
    println!("  list\t\t\tList all cookies");
    println!("  show (<name>|<idx>)\tList cookies for the specified host");
    println!("  delete (<name>|<idx>)\tDelete cookies for the specified host");
}
