use rusqlite::{params, Connection};
use std::env;
use std::io::{self, Write};
use std::path::PathBuf;

const DEFAULT_COOKIE_PATH: &str = ".local/share/qutebrowser/webengine/Cookies";

pub struct Cookie {
    conn: Connection,
}

impl Cookie {
    pub fn new() -> Self {
        let path = default_path().unwrap();
        let conn = Connection::open(path).unwrap();

        Self { conn }
    }

    pub fn print_hosts(&self) {
        let hosts = self.hosts().unwrap();

        for (i, host) in hosts.iter().enumerate() {
            println!("  [{}]\t{}", i, host);
        }
    }

    pub fn show_host(&self, host: &str) {
        if let Ok(idx) = host.parse() {
            self.show_host_idx(idx).unwrap()
        } else {
            self.show_host_name(host).unwrap()
        }
    }

    fn show_host_idx(&self, idx: usize) -> Result<(), rusqlite::Error> {
        let hosts = self.hosts()?;
        match hosts.get(idx) {
            Some(host) => self.show_host_name(host)?,
            None => eprintln!("Index {} doesn't exist.", idx),
        }
        Ok(())
    }

    fn show_host_name(&self, name: &str) -> Result<(), rusqlite::Error> {
        let mut stmt = self
            .conn
            .prepare("SELECT name, value FROM cookies WHERE host_key = ?")?;
        let rows = stmt
            .query_map(params![name], |row| {
                Ok((
                    row.get::<_, String>(0).unwrap(),
                    row.get::<_, String>(1).unwrap(),
                ))
            })?;

        let mut val = Vec::new();
        for r in rows {
            val.push(r?);
        }

        if val.is_empty() {
            eprintln!("Host not available: {}", name);
        } else {
            println!("Host: {}", name);
            for v in val {
                println!("  {}\n    {}", v.0, v.1);
            }
        }
        Ok(())
    }

    pub fn delete_host(&self, host: &str) {
        if let Ok(idx) = host.parse() {
            self.delete_host_idx(idx).unwrap()
        } else {
            self.delete_host_name(host).unwrap()
        }
    }

    fn delete_host_idx(&self, idx: usize) -> Result<(), rusqlite::Error> {
        let hosts = self.hosts()?;
        match hosts.get(idx) {
            Some(host) => self.delete_host_name(host)?,
            None => eprintln!("Index {} doesn't exist.", idx),
        }
        Ok(())
    }

    fn delete_host_name(&self, name: &str) -> Result<(), rusqlite::Error> {
        print!("Delete cookies for '{}'? (y/N) ", name);
        io::stdout().flush().unwrap();

        let mut input = String::new();
        match io::stdin().read_line(&mut input) {
            Ok(_) => {
                let input = input.trim();

                if input == "y" || input == "Y" {
                    self.conn
                        .execute("DELETE FROM cookies WHERE host_key = ?", params![name])
                        .and_then(|updated| {
                            if updated == 0 {
                                println!("There is no cookies for '{}'.", name);
                            } else {
                                println!("{} cookies for '{}' have been deleted.", updated, name);
                            }
                            Ok(())
                        })?
                } else {
                    println!("Aborted")
                }
            }
            Err(_) => println!("Interrupted"),
        }
        Ok(())
    }

    fn hosts(&self) -> Result<Vec<String>, rusqlite::Error> {
        let mut stmt = self.conn.prepare("SELECT DISTINCT host_key FROM cookies")?;
        let rows = stmt.query_map(params![], |row| row.get::<_, String>(0))?;

        let mut hosts = Vec::new();
        for host in rows {
            hosts.push(host?);
        }

        Ok(hosts)
    }
}

fn default_path() -> Result<PathBuf, env::VarError> {
    env::var("HOME").and_then(|s| Ok([&s, DEFAULT_COOKIE_PATH].iter().collect()))
}
